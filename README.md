# Weater Alert API

## Intallation and Setup

### Prerequesite - install poetry

**Linux, OSx**

`curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -`


**Windows powershell**

`Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -`


### Install dependencies

`poetry install`


### Install demo data

If you have make installe, you can use conveniance command:

`make setup`

If you do not have make installed on your system, run:

1. `cd weather && poetry run python manage.py migrate`
1. `cd weather && poetry run python manage.py create_demo_data`


## Running the app

Make sure that you have `WEATHER_API_KEY` set in your environment.

You can get the free one from [OpenWeather Map](https://home.openweathermap.org/api_keys)


**Starting webserver**
Start webserver with either:
 - `make start-webserver` OR
 - `cd weather && poetry run python manage.py runserver`


**Starting worker**
Start worker with either:
- `make start-worker` OR
- `poetry run python weather/worker.py`


If you successfuly generated demo data, you should be able to open url in your browser 
and login with username `admin` and password `admindemo`.

You should be able to use swagger UI to test and play with the endpoints. Alternatively you can 
use django admin.


## Running tests
You can run tests with either:
 - `make test` OR
 - `poetry run pytest weather`
 
 
## Notes about the rules
Currently only supported dimensions are:
  - temperature
  - pressure
  - humidity
  - wind_speed
  
  
Currently supported conditions are:
  - equal
  - lt
  - lte
  - gt
  - gte
 
