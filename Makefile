##
# Alert API
#
# @file
# @version 0.1


run-migrations:
	cd weather && poetry run python manage.py migrate


create-demo-data:
	cd weather && poetry run python manage.py create_demo_data


setup:	run-migrations create-demo-data


test:
	poetry run pytest weather


start-worker:
	poetry run python weather/worker.py


start-webserver:
	cd weather && poetry run python manage.py runserver


mypy:
	poetry run mypy weather --disallow-untyped-def --install-types


format:
	poetry run isort weather && poetry run black weather


flake:
	poetry run flake8 weather


complexity:
	poetry run xenon weather


check: complexity format mypy flake

# end
