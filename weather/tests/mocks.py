from typing import Any, Dict, List

GeocodingType = List[Dict[str, Any]]


class GeocodingServiceMock:
    _data: GeocodingType
    _api_key = "TEST_API_KEY"

    def get(self, address: str, *, limit: int = 5) -> GeocodingType:
        return self._data


def geocode_service_mock(data: GeocodingType) -> GeocodingServiceMock:
    mock = GeocodingServiceMock()
    mock._data = data
    return mock
