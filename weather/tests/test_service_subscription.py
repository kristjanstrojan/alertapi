import dependencies
import pytest
from db import factories, models
from service import subscription

from .mocks import geocode_service_mock

ADDRESS = "SOME_ADDRESS_TEST"
LAT = -40.0
LON = 40.0

GEOCODING = [
    {
        "name": ADDRESS,
        "country": "Some Country",
        "lat": LAT,
        "lon": LON,
    },
]

RULES = [
    {
        "dimension": "temperature",
        "condition": "gte",
        "value": 15,
    }
]


@pytest.mark.django_db
def test_create() -> None:
    EMAIL = "test@domain.com"

    user = factories.UserFactory.create()

    with dependencies.override("geocoding_service", geocode_service_mock(GEOCODING)):
        subscription.create(EMAIL, ADDRESS, RULES, user.id)

    assert models.AlertSubscription.objects.count() == 1

    sub = models.AlertSubscription.objects.first()

    assert sub.location.address == ADDRESS
