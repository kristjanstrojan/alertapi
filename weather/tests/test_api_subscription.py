import dependencies
from db import factories
from django.conf import settings
from rest_framework.test import APITransactionTestCase

from .mocks import geocode_service_mock

User = settings.AUTH_USER_MODEL


RULES = [{"dimension": "temperature", "condition": "lt", "value": 10.0}]

BASE_URL = "/api/subscription"

geocode_records = [
    {
        "name": "Some Address",
        "country": "Some Country",
        "lat": -10.0,
        "lon": 10.0,
    }
]


class SubscriptionApiTestCase(APITransactionTestCase):
    def setUp(self) -> None:
        factories.UserFactory.create()

    def login(self) -> None:
        self.client.login(username=factories.USER_USERNAME, password=factories.USER_USERNAME)

    def test_unauthorized(self) -> None:
        payload = {
            "email": "test@domain.com",
            "location": "London",
            "rules": RULES,
        }

        retrieve = self.client.get(BASE_URL)
        self.assertEqual(retrieve.status_code, 403)

        create = self.client.post(BASE_URL, payload)
        self.assertEqual(create.status_code, 403)

        get = self.client.get(f"{BASE_URL}/0")
        self.assertEqual(get.status_code, 403)

        put = self.client.put(f"{BASE_URL}/0", payload)
        self.assertEqual(put.status_code, 403)

        patch = self.client.patch(f"{BASE_URL}/0", payload)
        self.assertEqual(patch.status_code, 403)

        delete = self.client.patch(f"{BASE_URL}/0")
        self.assertEqual(delete.status_code, 403)

    def test_api(self) -> None:
        self.login()
        payload = {
            "email": "test@domain.com",
            "location": "London",
            "rules": RULES,
        }

        with dependencies.override("geocoding_service", geocode_service_mock(geocode_records)):
            response = self.client.post(BASE_URL, payload)
            self.assertEqual(response.status_code, 201)

            for _ in range(4):
                response_ = self.client.post(BASE_URL, payload)
                self.assertEqual(response_.status_code, 201)

        # LIST
        list_all = self.client.get(BASE_URL)
        self.assertEqual(list_all.status_code, 200)
        data = list_all.json()
        self.assertEqual(len(data), 5)

        # UPDATE
        updated_payload = {
            "email": "test@doain.com",
            "location": "New York City",
            "rules": [],
        }
        update_record_id = response.json()["id"]
        with dependencies.override("geocoding_service", geocode_service_mock(geocode_records)):
            update = self.client.put(f"{BASE_URL}/{update_record_id}", updated_payload)
        self.assertEqual(update.status_code, 200)
        # -- Asert that the record is updated
        updated = self.client.get(f"{BASE_URL}/{update_record_id}").json()
        self.assertEqual(updated["id"], update_record_id)
        self.assertEqual(updated["location"], "New York City")
        self.assertEqual(updated["rules"], [])

        # DELETE
        delete = self.client.delete(f"{BASE_URL}/{update_record_id}")
        self.assertEqual(delete.status_code, 204)
        list_all = self.client.get(BASE_URL).json()
        self.assertEqual(len(list_all), 4)
