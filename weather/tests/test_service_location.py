import dependencies
import pytest
from db import models
from service import location

from .mocks import geocode_service_mock


def test_get_geolocation_for_address() -> None:
    data = [
        {
            "name": "Some Address",
            "country": "Some Country",
            "lat": -10.0,
            "lon": 10.0,
        },
        {
            "name": "Some Address 2",
            "country": "Some Country 2",
            "lat": -20.0,
            "lon": 20.0,
        },
    ]
    with dependencies.override("geocoding_service", geocode_service_mock(data)):
        loc = location.get_geolocation_for_address("TEST ADDRESS")
        assert isinstance(loc, location.GeocodingRecord)
        assert loc.name == "Some Address"
        assert loc.country == "Some Country"
        assert loc.lat == -10.0
        assert loc.lon == 10.0


def test_invalid_data() -> None:
    data = [{"some_key": "A"}]

    with dependencies.override("geocoding_service", geocode_service_mock(data)):
        with pytest.raises(location.GeocodingError):
            location.get_geolocation_for_address("TEST ADDRESS")


@pytest.mark.django_db
def test_get_location_by_address() -> None:
    ADDRESS = "some_address"

    models.AlertLocation.objects.create(address=ADDRESS, lat=1, lon=2)

    loc = location.get_location_by_address(ADDRESS)

    assert loc.address == ADDRESS


@pytest.mark.django_db
def test_get_location_by_address__not_found() -> None:
    with pytest.raises(location.NoLocationError):
        location.get_location_by_address("NO LOCATION")


@pytest.mark.django_db
def test_get_or_create_location() -> None:
    EXISTING_ADDRESS = "EXISTING_ADDRESS"
    models.AlertLocation.objects.create(address=EXISTING_ADDRESS, lat=1, lon=2)

    NEW_ADDRESS = "NEW_ADDRESS"

    data = [
        {
            "name": "Some Address",
            "country": "Some Country",
            "lat": -40.0,
            "lon": 40.0,
        },
    ]
    with dependencies.override("geocoding_service", geocode_service_mock(data)):
        existing = location.get_or_create_location(EXISTING_ADDRESS)
        assert existing.address == EXISTING_ADDRESS
        assert existing.lat == 1
        assert existing.lon == 2

        new = location.get_or_create_location(NEW_ADDRESS)
        assert new.address == NEW_ADDRESS
        assert new.lat == -40.0
        assert new.lon == 40.0
