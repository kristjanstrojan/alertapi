from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    pass


class AlertLocation(models.Model):
    class Meta:
        db_table = "alert_location"

    address = models.CharField(max_length=150, unique=True)
    lat = models.FloatField()
    lon = models.FloatField()


class AlertSubscriber(models.Model):
    class Meta:
        db_table = "alert_subscriber"

    email = models.EmailField(max_length=320, unique=True)


class AlertSubscription(models.Model):
    class Meta:
        db_table = "alert_subscription"

    subscriber = models.ForeignKey(AlertSubscriber, on_delete=models.CASCADE)
    location = models.ForeignKey(AlertLocation, on_delete=models.PROTECT)
    rules = models.JSONField(default=dict)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="+"
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="+"
    )
