import factory
from django.conf import settings

User = settings.AUTH_USER_MODEL


USER_EMAIL = "test@domain.com"
USER_USERNAME = "user"
USER_PASSWORD = "user"


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ("username",)

    email = USER_EMAIL
    username = USER_USERNAME
    password = factory.PostGenerationMethodCall("set_password", USER_PASSWORD)

    is_superuser = False  # !
    is_staff = False  # !
    is_active = True
