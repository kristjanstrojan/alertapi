from django.contrib import admin

from . import models


class AlertLocationAdmin(admin.ModelAdmin):
    pass


class AlertSubscriptionAdmin(admin.ModelAdmin):
    pass


class AlertSubscriberAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.AlertLocation, AlertLocationAdmin)
admin.site.register(models.AlertSubscription, AlertSubscriptionAdmin)
admin.site.register(models.AlertSubscriber, AlertSubscriberAdmin)
