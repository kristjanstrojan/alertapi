from db import models
from django.core.management.base import BaseCommand
from service import subscription


class Command(BaseCommand):
    help = "Creates dummy demo data"

    def handle(self, *args, **options):  # type: ignore
        # Validate

        USERNAME = "admin"
        PASSWORD = "admindemo"
        EMAIL = "test@domain.com"
        superuser = models.User.objects.create_superuser(
            username=USERNAME,
            password=PASSWORD,
            email=EMAIL,
        )

        subscriptions = [
            (
                "London, GB",
                "subscription1@domain.com",
                [{"dimension": "temperature", "condition": "lt", "value": 30}],
            ),
            (
                "New York City, USA",
                "subscription2@domain.com",
                [
                    {
                        "dimension": "temperature",
                        "condition": "gte",
                        "value": 15,
                    }
                ],
            ),
        ]

        self.stdout.write("Creating subscriptions")
        for (address, email, rules) in subscriptions:
            subscription.create(email, address, rules, superuser.id)
            self.stdout.write("Subscription created: ")
            self.stdout.write(f"  - email: {email}")
            self.stdout.write(f"  - address: {address}")
            self.stdout.write("  - with rules:")
            for rule in rules:
                self.stdout.write(
                    f"    - {rule['dimension']} is {rule['condition']} to/than {rule['value']}"
                )

        self.stdout.write("Dummy data created.")
        self.stdout.write(
            " - Start worker process with `make start-worker` or `poetry run python worker.py`."
            "\n"
            " - Start webserver process with `make start-webserver` or "
            "`cd weather && poetry run python manage.py runserver`"
            "\n\n"
            "A demo user account was created: \n"
            f" - username: {USERNAME} \n"
            f" - password: {PASSWORD} \n"
        )
