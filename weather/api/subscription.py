import structlog
from db import models
from rest_framework import permissions, routers, serializers, viewsets
from rest_framework.request import Request
from rest_framework.response import Response
from service import constants, subscription

logger = structlog.get_logger("api")


class RuleSerializer(serializers.Serializer):
    dimension = serializers.ChoiceField(choices=constants.DIMENSIONS)
    condition = serializers.ChoiceField(choices=constants.CONDITIONS)
    value = serializers.FloatField()


class SubscriptionModelSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(source="subscriber.email")
    location = serializers.CharField(source="location.address")
    rules = RuleSerializer(many=True)
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())
    updated_by = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = models.AlertSubscription
        fields = [
            "id",
            "email",
            "location",
            "rules",
            "created_by",
            "updated_by",
            "created_at",
            "updated_at",
        ]


class SubscriptionCreateUpdateSerializer(serializers.Serializer):
    email = serializers.EmailField()
    location = serializers.CharField()
    rules = RuleSerializer(many=True)


class SubscriptionViewSet(viewsets.ModelViewSet):
    serializer_class = SubscriptionModelSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = models.AlertSubscription.objects.all()

    def create(self, request: Request) -> Response:
        serializer = SubscriptionCreateUpdateSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)

        try:
            record = subscription.create(
                serializer.validated_data["email"],
                serializer.validated_data["location"],
                serializer.validated_data["rules"],
                request.user.id,
            )
            response_serializer = SubscriptionModelSerializer(record)
            return Response(response_serializer.data, status=201)
        except Exception as e:
            logger.exception(e)
            return Response("Something went wrong", status=400)

    def update(self, request: Request, pk: int) -> Response:
        serializer = SubscriptionCreateUpdateSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        try:
            updated_record = subscription.update(
                pk,
                serializer.validated_data["email"],
                serializer.validated_data["location"],
                serializer.validated_data["rules"],
                request.user.id,
            )
            response_serializer = SubscriptionModelSerializer(updated_record)
            return Response(response_serializer.data, status=200)
        except Exception as e:
            logger.exception(e)
            return Response("Something went wrong", status=400)


router = routers.SimpleRouter(trailing_slash=False)
router.register("subscription", SubscriptionViewSet, "subscription")
