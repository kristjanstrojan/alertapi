from copy import deepcopy
from types import TracebackType
from typing import Any, Dict, Optional, Type


class _DependencyManager:
    def __init__(self) -> None:
        self._dependencies: Dict[str, Any] = {}

    def set(self, key: str, value: Any, *, override: bool = False) -> None:
        if key in self._dependencies and override is False:
            raise ValueError("Dependency already present. Use override=True to override it.")
        self._dependencies[key] = value

    def get(self, key: str) -> Any:
        return self._dependencies.get(key)


registry = _DependencyManager()


class override:
    def __init__(self, key: str, value: Any):
        self.key = key
        self.value = value
        self._backup = deepcopy(registry.get(self.key))

    def __enter__(self) -> None:
        registry.set(self.key, self.value, override=True)

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> None:
        registry.set(self.key, self._backup, override=True)
