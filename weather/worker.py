import functools
import multiprocessing as mp
import time
from typing import List, Optional

import structlog
from adjango import *  # noqa
from db import models
from service import weather

logger = structlog.get_logger("worker")


Task = functools.partial


def start() -> None:
    cpus = mp.cpu_count()
    tasks_queue: mp.Queue = mp.Queue()

    scheduler_ = mp.Process(target=scheduler, args=(tasks_queue,))
    scheduler_.start()

    logger.info("Scheduler started")

    workers = []

    for _ in range(cpus - 1):
        worker_ = mp.Process(target=worker, args=(tasks_queue,))
        workers.append(worker_)
        worker_.start()
    logger.info("Workers started", count=cpus - 1)


def scheduler(task_queue: mp.Queue) -> List[Task]:
    while True:
        locations = models.AlertLocation.objects.all()

        for location in locations:
            logger.info("Scheduler - location", location=location.address)
            task_queue.put(functools.partial(get_weather, location.id, location.lat, location.lon))

        time.sleep(10)


def worker(task_queue: mp.Queue) -> None:
    while True:
        task = task_queue.get()
        process_task(task, task_queue)


def process_task(func: Task, task_queue: mp.Queue) -> None:
    try:
        result = func()
    except Exception as e:
        logger.exception(e)

    if callable(result):
        task_queue.put(result)
    elif isinstance(result, list):
        for item in result:
            if callable(item):
                task_queue.put(item)


def get_weather(location_id: int, lat: float, lon: float) -> Optional[Task]:
    # Get locations
    try:
        logger.info("Task - get_weather", lat=lat, lon=lon)
        data = weather.get_weather_for_location(lat, lon)
        return functools.partial(process_weather_data, location_id, data)
    except Exception as e:
        logger.exception(e)
    return None


def process_weather_data(location_id: int, data: weather.Weather) -> Optional[List[Task]]:
    """
    Process weather data and determine the notifications
    """
    subscriptions = models.AlertSubscription.objects.filter(location_id=location_id)

    tasks = []

    for subscription in subscriptions:

        conditions = []

        for rule in subscription.rules:
            dimension = rule["dimension"]
            condition = rule["condition"]
            value = rule["value"]
            actual_value = getattr(data, dimension, None)
            logger.info(
                "Processing rule",
                dimension=dimension,
                condition=condition,
                value=value,
                actual_value=actual_value,
            )
            if actual_value is None:
                logger.error(
                    f"Could not retrieve dimension '{dimension}' " "from the weather data."
                )
                continue
            conditions.append(compare_condition(condition, value, actual_value))

        if all(conditions):
            tasks.append(
                functools.partial(
                    notify,
                    subscription.subscriber.email,
                    subscription.rules,
                    data,
                )
            )

    if len(tasks) > 0:
        return tasks
    return None


def notify(email: str, alert: str, data: weather.Weather) -> None:
    """
    Mocked notification task.
    """
    logger.info("Send notification", to=email, alert=alert, weather_data=data)


def compare_condition(condition: str, target_value: float, actual_value: float) -> bool:
    if condition == "equal":
        return actual_value == target_value
    if condition == "lt":
        return actual_value < target_value
    if condition == "lte":
        return actual_value <= target_value
    if condition == "gt":
        return actual_value > target_value
    if condition == "gte":
        return actual_value >= target_value
    else:
        raise ValueError(f"Condition '{condition}; is not implemented")


if __name__ == "__main__":

    start()
