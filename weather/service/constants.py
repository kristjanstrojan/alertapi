CONDITIONS = [
    "equal",
    "gt",
    "gte",
    "lt",
    "lte",
]


DIMENSIONS = [
    "temperature",
    "pressure",
    "humidity",
    "wind_speed",
]
