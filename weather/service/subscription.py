from typing import List

from db import models
from django.db import transaction
from service import location


def create(email: str, address: str, rules: List[dict], user_id: int) -> models.AlertSubscription:
    loc = location.get_or_create_location(address)
    subscriber = get_or_create_subscriber(email)

    record = models.AlertSubscription.objects.create(
        location=loc,
        subscriber=subscriber,
        rules=rules,
        created_by_id=user_id,
        updated_by_id=user_id,
    )
    return record


def update(
    subscription_id: int,
    email: str,
    address: str,
    rules: List[dict],
    user_id: int,
) -> models.AlertSubscription:
    loc = location.get_or_create_location(address)
    subscriber = get_or_create_subscriber(email)
    with transaction.atomic():
        record = models.AlertSubscription.objects.get(pk=subscription_id)
        record.location = loc
        record.subscriber = subscriber
        record.rules = rules
        record.updated_by_id = user_id
        record.save()
    return record


def get_or_create_subscriber(subscriber_email: str) -> models.AlertSubscriber:
    """
    Gets or creates subscriber
    """
    (record, _created) = models.AlertSubscriber.objects.get_or_create(email=subscriber_email)
    return record
