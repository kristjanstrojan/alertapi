from typing import Any, Dict, List

import dependencies
import requests
from db import models
from django.conf import settings
from pydantic import BaseModel, StrictFloat, StrictStr, ValidationError

GEOCODING_API_URI = "http://api.openweathermap.org/geo/1.0/direct"


class GeocodingError(Exception):
    pass


class NoLocationError(Exception):
    pass


class LocationError(Exception):
    pass


class GeocodingRecord(BaseModel):
    name: StrictStr
    country: StrictStr
    lat: StrictFloat
    lon: StrictFloat


class GeocodingService:
    def __init__(self, api_key: str) -> None:
        self._api_key = api_key

    def get(self, address: str, *, limit: int = 5) -> List[Dict[Any, Any]]:
        url = f"{GEOCODING_API_URI}?appid={self._api_key}&q={address}&limit={limit}"

        try:
            response = requests.get(url)
            if response.status_code != 200:
                raise GeocodingError(
                    f"Error response: {response.status_code}, message: {response.text}"
                )
        except requests.exceptions.RequestException as e:
            raise GeocodingError(str(e))

        data = response.json()

        return data


dependencies.registry.set("geocoding_service", GeocodingService(settings.WEATHER_API_KEY))


def get_geolocation_for_address(address: str) -> GeocodingRecord:
    """
    Get's the geolacation for address from api
    """
    geocoding_service = dependencies.registry.get("geocoding_service")

    data = geocoding_service.get(address)

    assert isinstance(data, list), "Expected data to be list"

    if len(data) < 1:
        raise GeocodingError("No geocoding match")

    try:
        record = GeocodingRecord.parse_obj(data[0])
        return record
    except ValidationError as e:
        raise GeocodingError(str(e))


def get_location_by_address(address: str) -> models.AlertLocation:
    try:
        record = models.AlertLocation.objects.get(address=address)
        return record
    except models.AlertLocation.DoesNotExist:
        raise NoLocationError


def create_location_for_address(address: str) -> models.AlertLocation:
    try:
        geocode = get_geolocation_for_address(address)
    except GeocodingError:
        raise LocationError("Cannot retrieve geocoding for location")

    record = models.AlertLocation.objects.create(address=address, lat=geocode.lat, lon=geocode.lon)

    return record


def get_or_create_location(address: str) -> models.AlertLocation:
    """
    Gets or creates the location and returns the
    id of the db record
    """
    try:
        record = get_location_by_address(address)
    except NoLocationError:
        record = create_location_for_address(address)
    return record
