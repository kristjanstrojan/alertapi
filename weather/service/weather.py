import dependencies
import requests
import structlog
from django.conf import settings
from pydantic import BaseModel

WEATHER_API_URI = "https://api.openweathermap.org/data/2.5/weather"


logger = structlog.get_logger("service.weather")


class WeatherServiceError(Exception):
    pass


class Weather(BaseModel):
    temperature: float
    pressure: float
    humidity: float
    wind_speed: float


class WeatherService:
    def __init__(self, api_key: str):
        self._api_key = api_key

    def get(self, lat: float, lon: float) -> requests.Response:
        url = f"{WEATHER_API_URI}?lat={lat}&lon={lon}&units=metric&appid={self._api_key}"
        response = requests.get(url)
        return response


dependencies.registry.set("weather_service", WeatherService(settings.WEATHER_API_KEY))
print("WAPI", settings.WEATHER_API_KEY)


def get_weather_for_location(lat: float, lon: float) -> Weather:
    weather_service = dependencies.registry.get("weather_service")

    response = weather_service.get(lat, lon)
    if response.status_code != 200:
        logger.error(
            "Request to weather service failed.",
            status_code=200,
            response=response.json(),
            url=response.request.url,
        )
        raise WeatherServiceError("Could not retrieve weather")

    data = response.json()

    record = Weather(
        temperature=data["main"]["temp"],
        pressure=data["main"]["pressure"],
        humidity=data["main"]["humidity"],
        wind_speed=data["wind"]["speed"],
    )

    return record
